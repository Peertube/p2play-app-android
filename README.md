# P2Play
P2Play is an unoficial Peertube android application.

[What is Peertube?](https://github.com/Chocobozzz/PeerTube/)

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/org.libre.agosto.p2play/)

## Screenshots

<img src="metadata/en-US/images/phoneScreenshots/1.jpg" alt="Screenshot" width="14%"/>
<img src="metadata/en-US/images/phoneScreenshots/2.jpg" alt="Screenshot" width="14%"/>
<img src="metadata/en-US/images/phoneScreenshots/3.jpg" alt="Screenshot" width="14%"/>
<img src="metadata/en-US/images/phoneScreenshots/4.jpg" alt="Screenshot" width="14%"/>
<img src="metadata/en-US/images/phoneScreenshots/5.jpg" alt="Screenshot" width="14%">/
<img src="metadata/en-US/images/phoneScreenshots/6.jpg" alt="Screenshot" width="14%"/>

## Realeases (apk's)

[All realeases are here](https://gitlab.com/agosto182/p2play/tags)

## Features

- Show recent, popular and local list of videos.
- Reproduce videos
- Login and register in your instance
- Show uploaded videos
- Subscribe to accounts
- Show your subscripcion videos
- Show your history
- Rate videos
- Show and make commentaries
- Splash screen
- Search videos
- Infinite scroll
- Share videos
- Report videos
- Peertube profiles
- Day/Night theme

## What to do? (on incomming updates)

- Playlists
- Manage subscriptions
- Better commentaries
- Account channels view
- Notifications

## Demostrations

P2play beta 0.6.0: https://fediverse.tv/w/suFPkm9zJstSrQU7WmVARv

**Dead links**

~~Demostration P2play Beta 0.2: https://peertube.video/videos/watch/730fa68e-32c4-4cdb-a7bb-1a819c9d3a46~~

~~Demostration P2Play Beta 0.1: https://peertube.video/videos/watch/2eb7b953-0b1b-4019-9300-817539f5f4e8~~

~~[Spanish] Demostracion P2Play Beta 0.1: https://peertube.video/videos/watch/d6a7da26-d3dd-43aa-ad5c-7d032603c848~~

## Contact
You can follow our accounts to get news and contact with the developer(s).

- WriteFreely (ActivityPub): https://personaljournal.ca/p2play/

## About
P2Play is made in Android Studio with Kotlin languaje.

![kotlin](https://weblizar.com/blog/wp-content/uploads/2017/11/Kotlin-A-New-Programming-Platform-For-Android-Developers.png)

### Developers
- Ivan Agosto: [https://mast.lat/@agosto182](https://mast.lat/@agosto182)

## License
This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see [license](./LICENSE.md)
