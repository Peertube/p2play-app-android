package org.libre.agosto.p2play.helpers

import android.view.View
import android.view.Window
import android.view.WindowManager

fun setFullscreen(window: Window) {
    val attrs = window.attributes
    attrs.flags = attrs.flags or WindowManager.LayoutParams.FLAG_FULLSCREEN
    attrs.flags = attrs.flags or WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
    window.attributes = attrs
    window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
}
