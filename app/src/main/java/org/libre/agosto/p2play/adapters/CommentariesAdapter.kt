package org.libre.agosto.p2play.adapters

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import org.libre.agosto.p2play.AccountActivity
import org.libre.agosto.p2play.ManagerSingleton
import org.libre.agosto.p2play.R
import org.libre.agosto.p2play.dialogs.ThreadDialog
import org.libre.agosto.p2play.models.CommentaryModel
import java.io.Serializable

class CommentariesAdapter(private val myDataset: ArrayList<CommentaryModel>) :
    RecyclerView.Adapter<CommentariesAdapter.ViewHolder>() {

    private lateinit var fragmentManager: FragmentManager

    fun setFragmentManager(manager: FragmentManager): CommentariesAdapter {
        this.fragmentManager = manager
        return this
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val userImg: ImageView
        val username: TextView
        val commentary: TextView
        val replyBtn: TextView
        val context: Context

        init {
            // Define click listener for the ViewHolder's View
            username = view.findViewById(R.id.userTxt)
            commentary = view.findViewById(R.id.userCommentary)
            userImg = view.findViewById(R.id.userCommentImg)
            replyBtn = view.findViewById(R.id.replyBtn)
            context = view.context
        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): ViewHolder {
        // create a new view
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.view_commentary, parent, false) as View

        // set the view's size, margins, paddings and layout parameters
        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.username.text = myDataset[position].username

        // holder.userImg.setOnClickListener {
        // val intent = Intent(holder.context, ChannelActivity::class.java)
        // intent.putExtra("channel", myDataset[position])
        // holder.context.startActivity(intent)
        // }

        if (myDataset[position].userImageUrl != "") {
            Picasso.get().load("https://" + ManagerSingleton.url + myDataset[position].userImageUrl).into(holder.userImg)
        }

        holder.commentary.text = Html.fromHtml(myDataset[position].commentary)
        holder.replyBtn.setOnClickListener { this.initRepliesDialog(myDataset[position]) }

        if (myDataset[position].replies > 0) {
            holder.replyBtn.text = holder.itemView.context.getString(R.string.see_replies, myDataset[position].replies)
        }

        // TODO: Support for view and account (is different than a video channel)
        holder.userImg.setOnClickListener {
            val intent = Intent(holder.context, AccountActivity::class.java)
            intent.putExtra("accountId", myDataset[position].getAccount())
            holder.context.startActivity(intent)
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = myDataset.size

    private fun initRepliesDialog(commentData: CommentaryModel) {
        val dialog = ThreadDialog()
        val bundle = Bundle()
        bundle.putSerializable("comment", commentData as Serializable)

        dialog.arguments = bundle
        dialog.fragmentManager2 = this.fragmentManager
        val transaction = fragmentManager.beginTransaction()
        // For a polished look, specify a transition animation.
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        // To make it fullscreen, use the 'content' root view as the container
        // for the fragment, which is always the root view for the activity.
        transaction
            .add(android.R.id.content, dialog)
            .addToBackStack("comments")
            .commit()
    }
}
