package org.libre.agosto.p2play

import android.content.Intent
import android.content.SharedPreferences
import android.os.AsyncTask
import android.os.Bundle
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager
import org.libre.agosto.p2play.ajax.Auth
import org.libre.agosto.p2play.databinding.ActivityHostBinding

class HostActivity : AppCompatActivity() {
    lateinit var settings: SharedPreferences
    lateinit var editor: SharedPreferences.Editor
    val client: Auth = Auth()
    private val db = Database(this)

    private lateinit var binding: ActivityHostBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHostBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        settings = PreferenceManager.getDefaultSharedPreferences(this)

        editor = settings.edit()

        binding.button.setOnClickListener {
            getKeys(binding.hostText.text.toString())
        }

        val host = settings.getString("hostP2play", "")
        val lastHost = settings.getString("last_host", "")
        if (host != "") {
            if (lastHost != host) {
                db.logout()
                ManagerSingleton.logout()
                getKeys(host!!)
            } else {
                ManagerSingleton.url = host
                startApp()
            }
        }
    }

    fun saveHost(host: String) {
        editor.putString("last_host", host)
        editor.putString("hostP2play", host)
        editor.apply()
        ManagerSingleton.toast(getString(R.string.finallyMsg), this)
        ManagerSingleton.url = host
        startApp()
    }

    private fun getKeys(hostText: String) {
        binding.button.isEnabled = false
        var host = hostText.toString()
        host = host.replace("http://", "")
        host = host.replace("https://", "")
        host = host.replace("/", "")
        ManagerSingleton.url = host
        AsyncTask.execute {
            if (Looper.myLooper() == null) {
                Looper.prepare()
            }

            val keys = client.getKeys()
            if (keys.client_id != "") {
                editor.putString("client_id", keys.client_id)
                editor.putString("client_secret", keys.client_secret)
                editor.apply()
                saveHost(host)
            } else {
                runOnUiThread {
                    ManagerSingleton.toast(getString(R.string.errorMsg), this)
                    binding.button.isEnabled = true
                }
            }
        }
    }

    private fun startApp() {
        runOnUiThread {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            this.finish()
        }
    }
}
