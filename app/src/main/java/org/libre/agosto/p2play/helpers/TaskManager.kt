package org.libre.agosto.p2play.helpers

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TaskManager<T> : ViewModel() {
    val result = MutableLiveData<T>()

    fun runTask(task: () -> T, callback: (T) -> Unit) {
        viewModelScope.launch {
            val data = withContext(Dispatchers.IO) {
                task()
            }
            result.postValue(data)
            callback(data)
        }
    }
}