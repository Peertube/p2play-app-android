package org.libre.agosto.p2play.models

import android.util.JsonReader

class DownloadFiles(
    var resolution: String = "",
    var url: String = ""
)

class StreamingModel(
    var playlistUrl: String = "",
    var segmentsSha256Url: String = "",
    var downloadFiles: ArrayList<DownloadFiles> = arrayListOf<DownloadFiles>()
) {
    fun parse(data: JsonReader) {
        data.beginObject()
        while (data.hasNext()) {
            val key = data.nextName()
            when (key.toString()) {
                "playlistUrl" -> {
                    this.playlistUrl = data.nextString()
                }
                "segmentsSha256Url" -> {
                    this.segmentsSha256Url = data.nextString()
                }
                "files" -> {
                    data.beginArray()
                    if (data.hasNext()) {
                        data.beginObject()
                        val downloadFile = DownloadFiles()
                        while (data.hasNext()) {
                            val key2 = data.nextName()
                            when (key2.toString()) {
                                "fileDownloadUrl" -> downloadFile.url = data.nextString()
                                "resolution" -> {
                                    data.beginObject()
                                    while(data.hasNext()) {
                                        val keyRes = data.nextName()
                                        when (keyRes!!) {
                                            "label" -> downloadFile.resolution = data.nextString()
                                            else -> data.skipValue()
                                        }
                                    }
                                    data.endObject()
                                }
                                else -> data.skipValue()
                            }
                        }
                        this.downloadFiles.add(downloadFile)
                        data.endObject()
                    }
                    while (data.hasNext()) {
                        data.skipValue()
                    }
                        data.endArray()
                }
                else -> data.skipValue()
            }
        }
        data.endObject()
    }
}
