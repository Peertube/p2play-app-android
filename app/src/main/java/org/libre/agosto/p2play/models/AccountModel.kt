package org.libre.agosto.p2play.models

import java.io.Serializable

data class AccountAvatar (
    val path: String
)

class AccountModel (
    val name: String,
    val url: String,
    val host: String,
    val avatars: ArrayList<AccountAvatar>,
    val displayName: String,
    val description: String
): Serializable