package org.libre.agosto.p2play.adapters

import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import org.libre.agosto.p2play.AccountActivity
import org.libre.agosto.p2play.ChannelActivity
import org.libre.agosto.p2play.ManagerSingleton
import org.libre.agosto.p2play.R
import org.libre.agosto.p2play.ajax.Actions
import org.libre.agosto.p2play.models.ChannelModel

class ChannelAdapter(private val myDataset: ArrayList<ChannelModel>): RecyclerView.Adapter<ChannelAdapter.ViewHolder>() {
    private val actionsService = Actions()
    lateinit var parent: FragmentActivity

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        // Define click listener for the ViewHolder's View
        val channelImage: ImageView = view.findViewById(R.id.channelImage)
        val channelName: TextView = view.findViewById(R.id.channelName)
        val channelDescription: TextView = view.findViewById(R.id.channelDescription)
        val subscribeButton: Button = view.findViewById(R.id.subscribeBtn)
        val context: Context = view.context
        var isSubscribed: Boolean = false
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChannelAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.view_channel, parent, false) as View

        return ViewHolder(view)
    }

    override fun getItemCount() = myDataset.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.channelName.text = myDataset[position].name
        holder.channelDescription.text = myDataset[position].description

        if (myDataset[position].channelImg != "") {
            Picasso.get().load("https://${ManagerSingleton.url}${myDataset[position].channelImg}").into(holder.channelImage)
        } else {
            holder.channelImage.setImageResource(R.drawable.default_avatar)
        }

        if (ManagerSingleton.user.status == 1) {
            getSubscription(myDataset[position], holder)
        } else {
            holder.subscribeButton.visibility = View.GONE
        }

        holder.subscribeButton.setOnClickListener {
            this.subscribeAction(myDataset[position], holder)
        }

        holder.channelImage.setOnClickListener {
            this.launchChannelActivity(myDataset[position].getAccount())
        }
        holder.channelName.setOnClickListener {
            this.launchChannelActivity(myDataset[position].getAccount())
        }
    }

    private fun getSubscription(channel: ChannelModel, holder: ViewHolder) {
        AsyncTask.execute {
            holder.isSubscribed = actionsService.getSubscription(ManagerSingleton.token.token, channel.getAccount())
            parent.runOnUiThread {
                if (holder.isSubscribed) {
                    holder.subscribeButton.text = parent.getText(R.string.unSubscribeBtn)
                } else {
                    holder.subscribeButton.text = parent.getText(R.string.subscribeBtn)
                }
            }
        }
    }

    private fun subscribe(channel: ChannelModel, holder: ViewHolder) {
        AsyncTask.execute {
            val res = actionsService.subscribe(ManagerSingleton.token.token, channel.getAccount())
            parent.runOnUiThread {
                if (res == 1) {
                    holder.subscribeButton.text = parent.getString(R.string.unSubscribeBtn)
                    ManagerSingleton.toast(parent.getString(R.string.subscribeMsg), parent)
                    getSubscription(channel, holder)
                } else {
                    ManagerSingleton.toast(parent.getString(R.string.errorMsg), parent)
                }
            }
        }
    }

    private fun unSubscribe(channel: ChannelModel, holder: ViewHolder) {
        AsyncTask.execute {
            val res = actionsService.unSubscribe(ManagerSingleton.token.token, channel.getAccount())
            parent.runOnUiThread {
                if (res == 1) {
                    holder.subscribeButton.text = parent.getString(R.string.subscribeBtn)
                    ManagerSingleton.toast(parent.getString(R.string.unSubscribeMsg), parent)
                    getSubscription(channel, holder)
                } else {
                    ManagerSingleton.toast(parent.getString(R.string.errorMsg), parent)
                }
            }
        }
    }

    private fun subscribeAction(channel: ChannelModel, holder: ViewHolder) {
        if (holder.isSubscribed) {
            unSubscribe(channel, holder)
        } else {
            subscribe(channel, holder)
        }
    }

    private fun launchChannelActivity (channelId: String) {
        val intent = Intent(parent, ChannelActivity::class.java)
            intent.putExtra("channel", channelId)
            parent.startActivity(intent)

    }
}